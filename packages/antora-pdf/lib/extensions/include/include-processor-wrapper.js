'use strict'

const Opal = global.Opal

const IncludeProcessor = require('./include-processor')
const resolveIncludeFile = require('./resolve-include-file')

module.exports.register = (registry, {file, contentCatalog, config}) => {
  // console.log(`registering ainclude IncludeProcessor wrapper for ${file.src.relative}`)

  const callback = (doc, target, cursor) => {
    const vfsContext = file.vfsContext
    if (vfsContext) {
      // console.log(`resolving ${target} in context of vfsContext ${vfsContext.current().token}`)
      return resolveIncludeFile(target, vfsContext.current().file, cursor, contentCatalog)
    } else {
      // console.log(`resolving ${target} in context of original file`)
      return resolveIncludeFile(target, file, cursor, contentCatalog)
    }
  }
  registry.prefer('include_processor', IncludeProcessor.$new(callback))
}
