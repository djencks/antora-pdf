'use strict'

const picomatch = require('picomatch')
const parseResoureId = require('./parse-resource-id')

function classifyPdfs (pdfFiles, contentCatalog, pdfPages, suppressHtml) {
  const pages = pdfFiles.reduce((pages, resourceId) => {
    const { component, version, module, family, relative } = parseResoureId(resourceId)
    const filter = {}
    component && (filter.component = component)
    version && (filter.version = version)
    module && (filter.module = module)
    family && (filter.family = family)
    relative && (relative.indexOf('*') === -1) && (filter.relative = relative)
    const relCompare = relative && (relative.indexOf('*') > -1) ? picomatch(relative) : () => true

    contentCatalog.findBy(filter)
      .filter((page) => relCompare(page))
      .forEach((page) => pages.add(page))
    return pages
  }, new Set())
  pages.forEach((page) => pdfPages.push(page))
  console.log(`printing ${pdfPages.length} configured pdf pages`)
  if (suppressHtml) {
    contentCatalog.getPages().forEach((page) => {
      delete page.out
    })
  }
}

module.exports = classifyPdfs
