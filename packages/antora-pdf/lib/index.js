'use strict'

/**
 * Pdf plugin for Antora-pdf
 *
 * Sets up an Antora plugin to generate pdfs.
 *
 * @namespace @djencks/antora-pdf-plugin
 */
module.exports = require('./pdf-plugin')
