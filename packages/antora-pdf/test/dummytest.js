/* eslint-env mocha */
'use strict'

const { describe } = require('mocha')
const { expect } = require('chai')

const TOC_UL = /<ul class="sectlevel\d*">/g
// function hackTOC (baseConverter, node, opts) {
//   var toc = baseConverter.$convert(node, 'outline', opts)
//   toc = toc.replace(TOC_UL, '</li><li>$&')
//   return toc.slice(9)
// }

describe('test', () => {
  it('toc-re', () => {
    const toc = `<ul class="sectlevel1">
<li><a href="#_acknowledgements">Acknowledgements</a>
<ul class="sectlevel2">
<li><a href="#_revision_history">Revision History</a></li>
</ul>
</li>
<li><a href="#_preface">Preface</a>
<ul class="sectlevel2">
`
    const r = toc.replace(TOC_UL, '</li><li>$&')
    expect(r).to.equal(`</li><li><ul class="sectlevel1">
<li><a href="#_acknowledgements">Acknowledgements</a>
</li><li><ul class="sectlevel2">
<li><a href="#_revision_history">Revision History</a></li>
</ul>
</li>
<li><a href="#_preface">Preface</a>
</li><li><ul class="sectlevel2">
`)
  })
})
