/* eslint-env mocha */
'use strict'

const { describe } = require('mocha')
const { expect } = require('chai')

// const { convertPageRef, convertImageRef, initializeTopLevel } = require('../lib/extensions/xref/convert-ref')
const { convertImageRef } = require('../lib/extensions/xref/convert-ref')

const mockContentCatalog = require('./antora-mock-content-catalog')

function makeKey (src) {
  return `${src.version}@${src.component}:${src.module}:${src.family}$${src.relative}`
}

const topPageRoot = {
  pub: { url: '/component-a/4.0/page.html', moduleRootPath: '.' },
  key: ({ version = '4.0', component = 'component-a', module = 'ROOT', relative = 'page.adoc' }) => makeKey({ version, component, module, family: 'page', relative }),
  // '4.0@component-a:ROOT:page$page.adoc',
}

const topPageModuleA = {
  pub: { url: '/component-a/4.0/module-a/page.html', moduleRootPath: '.' },
  key: ({ version = '4.0', component = 'component-a', module = 'module-a', relative = 'page.adoc' }) => makeKey({ version, component, module, family: 'page', relative }),
  // key: '4.0@component-a:module-a:page$page.adoc',
}

const topPageModuleATopic = {
  pub: { url: '/component-a/4.0/module-a/topic/page.html', moduleRootPath: '..' },
  key: ({ version = '4.0', component = 'component-a', module = 'module-a', relative = 'topic/page.adoc' }) => makeKey({ version, component, module, family: 'page', relative }),
  // key: '4.0@component-a:module-a:page$topic/page.adoc',
}

describe('path tests', () => {
  var contentCatalog

  beforeEach(() => {
    contentCatalog = mockContentCatalog(
      ['component-a', 'component-b'].reduce((accum, component) => {
        ['4.0', '4.5'].forEach((version) =>
          ['module-a', 'module-b'].forEach((module) =>
            ['page', 'image'].forEach((family) =>
              ['', 'topic/'].forEach((topic) => {
                accum.push({
                  component,
                  version,
                  module,
                  family,
                  relative: `${topic}${family}-a.${family === 'page' ? 'adoc' : 'svg'}`,
                })
              }
              )
            )
          )
        )
        return accum
      }, [])
    )
  })

  describe('from top page', () => {
    it('imageref same module-a', () => {
      expect(convertImageRef('image-a.svg', topPageModuleA.key({}), topPageModuleA, contentCatalog)).to.equal('_images/image-a.svg')
    })

    it('imageref same module-a, page topic', () => {
      expect(convertImageRef('image-a.svg', topPageModuleATopic.key({}), topPageModuleATopic, contentCatalog)).to.equal('../_images/image-a.svg')
    })

    it('imageref same module-a, image topic', () => {
      expect(convertImageRef('topic/image-a.svg', topPageModuleA.key({}), topPageModuleA, contentCatalog)).to.equal('_images/topic/image-a.svg')
    })

    it('imageref same module-a, page and image topic', () => {
      expect(convertImageRef('topic/image-a.svg', topPageModuleATopic.key({}), topPageModuleATopic, contentCatalog)).to.equal('../_images/topic/image-a.svg')
    })

    it('imageref ROOT to module-a', () => {
      expect(convertImageRef('module-a:image-a.svg', topPageRoot.key({}), topPageRoot, contentCatalog)).to.equal('module-a/_images/image-a.svg')
    })

    it('imageref ROOT to module-a, version', () => {
      expect(convertImageRef('4.5@module-a:image-a.svg', topPageRoot.key({}), topPageRoot, contentCatalog)).to.equal('../4.5/module-a/_images/image-a.svg')
    })

    it('imageref ROOT to module-a, component', () => {
      expect(convertImageRef('4.5@component-b:module-a:image-a.svg', topPageRoot.key({}), topPageRoot, contentCatalog)).to.equal('../../component-b/4.5/module-a/_images/image-a.svg')
    })
  })

  describe('from same dir as top page', () => {
    it('imageref same module-a', () => {
      expect(convertImageRef('image-a.svg', topPageModuleA.key({ relative: 'page-b.adoc' }), topPageModuleA, contentCatalog)).to.equal('_images/image-a.svg')
    })

    it('imageref same module-a, page topic', () => {
      expect(convertImageRef('image-a.svg', topPageModuleATopic.key({ relative: 'topic/page-b.adoc' }), topPageModuleATopic, contentCatalog)).to.equal('../_images/image-a.svg')
    })

    it('imageref same module-a, image topic', () => {
      expect(convertImageRef('topic/image-a.svg', topPageModuleA.key({ relative: 'page-b.adoc' }), topPageModuleA, contentCatalog)).to.equal('_images/topic/image-a.svg')
    })

    it('imageref same module-a, page and image topic', () => {
      expect(convertImageRef('topic/image-a.svg', topPageModuleATopic.key({ relative: 'topic/page-b.adoc' }), topPageModuleATopic, contentCatalog)).to.equal('../_images/topic/image-a.svg')
    })

    it('imageref ROOT to module-a', () => {
      expect(convertImageRef('module-a:image-a.svg', topPageRoot.key({ relative: 'page-b.adoc' }), topPageRoot, contentCatalog)).to.equal('module-a/_images/image-a.svg')
    })

    it('imageref ROOT to module-a, version', () => {
      expect(convertImageRef('4.5@module-a:image-a.svg', topPageRoot.key({ relative: 'page-b.adoc' }), topPageRoot, contentCatalog)).to.equal('../4.5/module-a/_images/image-a.svg')
    })

    it('imageref ROOT to module-a, component', () => {
      expect(convertImageRef('4.5@component-b:module-a:image-a.svg', topPageRoot.key({ relative: 'page-b.adoc' }), topPageRoot, contentCatalog)).to.equal('../../component-b/4.5/module-a/_images/image-a.svg')
    })
  })

  describe('from different topic as top page', () => {
    const pageSpec = { relative: 'topic-b/page-b.adoc' }
    it('imageref same module-a', () => {
      expect(convertImageRef('image-a.svg', topPageModuleA.key(pageSpec), topPageModuleA, contentCatalog)).to.equal('_images/image-a.svg')
    })

    it('imageref same module-a, page topic', () => {
      expect(convertImageRef('image-a.svg', topPageModuleATopic.key(pageSpec), topPageModuleATopic, contentCatalog)).to.equal('../_images/image-a.svg')
    })

    it('imageref same module-a, image topic', () => {
      expect(convertImageRef('topic/image-a.svg', topPageModuleA.key(pageSpec), topPageModuleA, contentCatalog)).to.equal('_images/topic/image-a.svg')
    })

    it('imageref same module-a, page and image topic', () => {
      expect(convertImageRef('topic/image-a.svg', topPageModuleATopic.key(pageSpec), topPageModuleATopic, contentCatalog)).to.equal('../_images/topic/image-a.svg')
    })

    it('imageref ROOT to module-a', () => {
      expect(convertImageRef('module-a:image-a.svg', topPageRoot.key(pageSpec), topPageRoot, contentCatalog)).to.equal('module-a/_images/image-a.svg')
    })

    it('imageref ROOT to module-a, version', () => {
      expect(convertImageRef('4.5@module-a:image-a.svg', topPageRoot.key(pageSpec), topPageRoot, contentCatalog)).to.equal('../4.5/module-a/_images/image-a.svg')
    })

    it('imageref ROOT to module-a, component', () => {
      expect(convertImageRef('4.5@component-b:module-a:image-a.svg', topPageRoot.key({ relative: 'topic-b/page-b.adoc' }), topPageRoot, contentCatalog)).to.equal('../../component-b/4.5/module-a/_images/image-a.svg')
    })
  })

  describe('from different module as top page', () => {
    const pageSpec = { module: 'module-b' }
    it('imageref same module-a', () => {
      expect(convertImageRef('image-a.svg', topPageModuleA.key(pageSpec), topPageModuleA, contentCatalog)).to.equal('../module-b/_images/image-a.svg')
    })

    it('imageref same module-a, page topic', () => {
      expect(convertImageRef('image-a.svg', topPageModuleATopic.key(pageSpec), topPageModuleATopic, contentCatalog)).to.equal('../../module-b/_images/image-a.svg')
    })

    it('imageref same module-a, image topic', () => {
      expect(convertImageRef('topic/image-a.svg', topPageModuleA.key(pageSpec), topPageModuleA, contentCatalog)).to.equal('../module-b/_images/topic/image-a.svg')
    })

    it('imageref same module-a, page and image topic', () => {
      expect(convertImageRef('topic/image-a.svg', topPageModuleATopic.key(pageSpec), topPageModuleATopic, contentCatalog)).to.equal('../../module-b/_images/topic/image-a.svg')
    })

    it('imageref ROOT to module-a', () => {
      expect(convertImageRef('module-a:image-a.svg', topPageRoot.key(pageSpec), topPageRoot, contentCatalog)).to.equal('module-a/_images/image-a.svg')
    })

    it('imageref ROOT to module-a, version', () => {
      expect(convertImageRef('4.5@module-a:image-a.svg', topPageRoot.key(pageSpec), topPageRoot, contentCatalog)).to.equal('../4.5/module-a/_images/image-a.svg')
    })

    it('imageref ROOT to module-a, component', () => {
      expect(convertImageRef('4.5@component-b:module-a:image-a.svg', topPageRoot.key({ relative: 'topic-b/page-b.adoc' }), topPageRoot, contentCatalog)).to.equal('../../component-b/4.5/module-a/_images/image-a.svg')
    })
  })

  describe('from different version as top page', () => {
    const pageSpec = { version: '4.5' }
    it('imageref same module-a', () => {
      expect(convertImageRef('image-a.svg', topPageModuleA.key(pageSpec), topPageModuleA, contentCatalog)).to.equal('../../4.5/module-a/_images/image-a.svg')
    })

    it('imageref same module-a, page topic', () => {
      expect(convertImageRef('image-a.svg', topPageModuleATopic.key(pageSpec), topPageModuleATopic, contentCatalog)).to.equal('../../../4.5/module-a/_images/image-a.svg')
    })

    it('imageref same module-a, image topic', () => {
      expect(convertImageRef('topic/image-a.svg', topPageModuleA.key(pageSpec), topPageModuleA, contentCatalog)).to.equal('../../4.5/module-a/_images/topic/image-a.svg')
    })

    it('imageref same module-a, page and image topic', () => {
      expect(convertImageRef('topic/image-a.svg', topPageModuleATopic.key(pageSpec), topPageModuleATopic, contentCatalog)).to.equal('../../../4.5/module-a/_images/topic/image-a.svg')
    })

    it('imageref ROOT to module-a', () => {
      expect(convertImageRef('module-a:image-a.svg', topPageRoot.key(pageSpec), topPageRoot, contentCatalog)).to.equal('../4.5/module-a/_images/image-a.svg')
    })

    it('imageref ROOT to module-a, version', () => {
      expect(convertImageRef('4.5@module-a:image-a.svg', topPageRoot.key(pageSpec), topPageRoot, contentCatalog)).to.equal('../4.5/module-a/_images/image-a.svg')
    })

    it('imageref ROOT to module-a, component', () => {
      expect(convertImageRef('4.5@component-b:module-a:image-a.svg', topPageRoot.key({ relative: 'topic-b/page-b.adoc' }), topPageRoot, contentCatalog)).to.equal('../../component-b/4.5/module-a/_images/image-a.svg')
    })
  })

  describe('from different component as top page', () => {
    const pageSpec = { component: 'component-b' }
    it('imageref same module-a', () => {
      expect(convertImageRef('image-a.svg', topPageModuleA.key(pageSpec), topPageModuleA, contentCatalog)).to.equal('../../../component-b/4.0/module-a/_images/image-a.svg')
    })

    it('imageref same module-a, page topic', () => {
      expect(convertImageRef('image-a.svg', topPageModuleATopic.key(pageSpec), topPageModuleATopic, contentCatalog)).to.equal('../../../../component-b/4.0/module-a/_images/image-a.svg')
    })

    it('imageref same module-a, image topic', () => {
      expect(convertImageRef('topic/image-a.svg', topPageModuleA.key(pageSpec), topPageModuleA, contentCatalog)).to.equal('../../../component-b/4.0/module-a/_images/topic/image-a.svg')
    })

    it('imageref same module-a, page and image topic', () => {
      expect(convertImageRef('topic/image-a.svg', topPageModuleATopic.key(pageSpec), topPageModuleATopic, contentCatalog)).to.equal('../../../../component-b/4.0/module-a/_images/topic/image-a.svg')
    })

    it('imageref ROOT to module-a', () => {
      expect(convertImageRef('module-a:image-a.svg', topPageRoot.key(pageSpec), topPageRoot, contentCatalog)).to.equal('../../component-b/4.0/module-a/_images/image-a.svg')
    })

    it('imageref ROOT to module-a, version', () => {
      expect(convertImageRef('4.5@module-a:image-a.svg', topPageRoot.key(pageSpec), topPageRoot, contentCatalog)).to.equal('../../component-b/4.5/module-a/_images/image-a.svg')
    })

    it('imageref ROOT to module-a, component', () => {
      expect(convertImageRef('4.5@component-b:module-a:image-a.svg', topPageRoot.key({ relative: 'topic-b/page-b.adoc' }), topPageRoot, contentCatalog)).to.equal('../../component-b/4.5/module-a/_images/image-a.svg')
    })
  })
})
