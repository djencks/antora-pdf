= {extension}-ui ui bundle extension
:extension: pdf
:extension-version: 0.0.3
:source-repository: https://gitlab.com/djencks/antora-{extension}
:description: This adds the layout, css, and js needed for pdf rendering using paged.js on Chromium/Chrome.

WARNING: This project uses `@djencks/antora-ui-builder` which is not an official part of Antora.
The ui-builder project is derived from the non-src parts of the `@antora/ui-default` project, but may not be up to date.
See https://gitlab.com/antora/antora-ui-default/-/issues/110[] and https://gitlab.com/antora/antora-ui-default/-/issues/111[].
If you construct a ui-builder project incorporating the ui additions from this project, you should expect several incompatible changes before the ui-builder concept becomes part of Antora (if it ever does).

== Description

{description}

The code is located at link:{source-repository}[]

== Usage

See the documentation for the (experimental) ui-builder at https://gitlab.com/djencks/antora-ui-builder.

To build a ui bundle based on the antora-ui-default sources with the additions from this extension, run `npm run build`.
It may be possible to run `gulp` directly but it is possible that will use an incompatible globally installed gulp.

To combine the UI elements from this extension with other elements, using `@djencks/antora-ui-builder`, set up a ui project.
The simplest way to do this is with `@djencks/antora-schematics`:

[source,console]
----
npm i -g @djencks/antora-schematics
antora-schematics uiExtension --help
# you may wish to examine the options
antora-schematics uiExtension --path my-ui --extensionName my-ui --namespace my-corp
----

This will set up a UI project in a new git repository under `my-ui`.

If you do not use the schematic, consult the ui-builder documentation.
Much of the following is done by the schematic.

* Include in your `antora-ui.yml` a clause such as:

[source,yml,subs=+attributes]
----
sources:
  - path: '@antora/ui-default' <1>
  - path: '@djencks/lunr-ui' <2>
  - path: '@djencks/{extension}-ui' <3>
  - path: ./ <4>
----
<1> The 'base' UI to extend.
Unless you have extraordinary modifications to the antora-ui-default, I recommend using the default.
<2> (Optional) Other external UI extensions.
These need to be listed in your package.json.
<3> This UI extension.
<4> Local UI extensions.

Set up your UI project as a ui builder project by including in the `package.json`

[source,json,subs="+attributes"]
----
{
  "name": "...",
  "version": "...",
  "description": "...",
  "main": "gulpfile.js",
  "files": [
    "src/**/*",
    "build/**/*"
  ],
  "scripts": {
    "build": "gulp"
  },
  "devDependencies": {
    "@djencks/antora-ui-builder": "https://experimental-repo.s3-us-west-1.amazonaws.com/djencks-antora-ui-builder-v0.0.6.tgz",
    "@antora/ui-default": "git+https://gitlab.com/antora/antora-ui-default.git",
    "@djencks/{extension}-ui": "https://experimental-repo.s3-us-west-1.amazonaws.com/djencks-{extension}-ui-v{extension-version}.tgz"
  }
}
----

Also include any other UI extensions you are using, such as, as in the `antora-ui.yml` sample, `@djencks/lunr-ui`.
Note that you need to add these to the devDependencies as well: in this case
[source,json]
----
    "@djencks/lunr-ui": "https://experimental-repo.s3-us-west-1.amazonaws.com/djencks-lunr-ui-v0.0.3.tgz"
----

Build your UI project with `npm run build`.

The output bundle will be under `build/`.
